#include <iostream> 
#include <string>
using namespace std;

struct card_struct
{
    int number; //инвентарный номер книги
    int department_n; //номер отдела
    string author; //автор
    string name; //название
    bool refund; //вернули книгу или нет
};


class Person
{
public:
    Person()
    {
        _philologist = (rand()%2 == 0);
    }
    void Live();
    void Laugh();
    void Love();
    bool isPhilologist();
private: 
    bool _philologist;
protected:
};


bool Person::isPhilologist()
{
    return _philologist;
}

void Person::Live()
{
    cout << "Live" << endl;
}

void Person::Laugh()
{
    cout << "Laugh" << endl;
}

void Person::Love()
{
    cout << "Love" << endl;
}

class Book
{
public:
    explicit Book(int number, string author, string name, int department_n)
    {
        _number = number;
        _author = author;
        _name = name;
        _department_n = department_n;
        _state = true;
    }
    ~Book() = default;
    void set_state(bool st);
    bool get_state();
    card_struct get_inf();
private:
    int _number;
    string _author;
    string _name;
    int _department_n; //номер отдела, где хранится книга
    bool _state; //вернули книгу (true) или нет (false)
};

class eBook : public Book
{
public:
    eBook(int number, string author, string name, int department_n) :
        Book(number, author, name, department_n) {}
private:

protected:
};

card_struct Book::get_inf()
{
    card_struct cur;
    cur.author = _author;
    cur.name = _name;
    cur.number = _number;
    cur.department_n = _department_n;
    cur.refund = false; 
    return(cur);
}

void Book::set_state(bool st)
{
    _state = st;
}

bool Book::get_state()
{
    return _state;
}


class Card
{
public:
    Card()
    {
        _last = -1;
    }
    ~Card() = default;
    void set_last(int l);
    card_struct get_last_inf();
    void clean();
    void add(Book book);
    void set_state(bool st);
    friend bool state(Card &card);
    friend int last(Card& card);
private:
    card_struct _card[15]; //карточка, куда вписываются книги
    int _last; //номер последней записи
};

void Card::set_state(bool st)
{
    _card[_last].refund = st;
}

void Card::set_last(int l)
{
    _last = l;
}


bool state(Card& card)
{
    if (card._last == -1)
    {
        return true;
    }
    return(card._card[card._last].refund);
}

int last(Card& card)
{
    return(card._last);
}


card_struct Card::get_last_inf()
{
    return(_card[_last]);
}


void Card::add(Book book)
{
    _card[_last] = book.get_inf();
}


class Reader : public Person
{
public:
    Reader() = default;
    ~Reader() = default;
    void get_last_inf();
    void take_book(Book &&book);
    void take_book_forever(Book&& book);
    void return_book(Book &book);
    friend int last(Reader &reader);
private:
     Card _card = Card();
};

void Reader::take_book_forever(Book&& book)
{
    //статусы человека и книги не меняются. для билиотеки книга перестает существовать
}

void Reader::get_last_inf()
{
    cout << _card.get_last_inf().author << " " << _card.get_last_inf().name << endl;
}

void Card::clean()
{
    _last = -1;
}

int last(Reader& reader)
{
    return(last(reader._card));
}


void Reader::take_book(Book &&book)
{
    if (book.get_state())
    {
        if (state(this->_card) || isPhilologist()) //проверим, не должник ли человек
        {
            if (last(this->_card) == 14) //если карточка переполнена, то заводим новую
            {
                _card.clean();
            }
            _card.set_last(last(this->_card) + 1);
            _card.add(book); //добавляем новую книгу в карточку
            book.set_state(false);
            
        }
        else
        {
            cout << "Return other book first" << endl;
        }
    }
    else
    {
        cout << "The book is busy" << endl;
    }
}


bool compare(card_struct x, card_struct y)
{
    if (x.author != y.author)
    {
        return false;
    }
    if (x.department_n != y.department_n)
    {
        return false;
    }
    if (x.name != y.name)
    {
        return false;
    }
    if (x.number != y.number)
    {
        return false;
    }
    return true;
}


void Reader::return_book(Book &book)
{
    //проверяем, совпадает ли книга в списке
    if (compare(book.get_inf(), _card.get_last_inf()))
    {
        //меняем статус книги
        book.set_state(true);
        //меняем статус человека
        _card.set_state(true);
    }
    else
    {
        cout << "Not right book" << endl;
    }
}



int main()
{
    // создадим нового уже не просто читателя, а человека Диану
    Reader Diana = Reader();
    // пусть в нашей библиотеке c номером подразделения 84 есть следующие книги:
    Book b1 = Book(1295, "Theodore Dreiser", "Genius", 84);
    Book b2 = Book(2784, "Jodi Picoult", "Nineteen Minutes", 84);
    Book b3 = Book(3884, "Vladimir Nabokov", "Mary", 84);
    Book b4 = Book(1085, "Yevgeny Zamyatin", "We", 84);
    Book b5 = Book(4400, "Erich Maria Remarque", "The Night in Lisbon", 84);
    Book b6 = Book(3601, "James Joyce", "Ulysses", 84);
    Book b7 = Book(3601, "Fyodor Dostoevsky", "Crime and Punishment", 84);
    Book b8 = Book(2555, "Leonid Andreev", "Iuda Iskariot", 84);
    Book b9 = Book(2655, "Joe Abercrombie", "The Blade Itself", 84);
    // пусть Диана хочет взять первую книгу
    Diana.take_book(move(b1));
    // пусть теперь Диана хочет взять еще одну книгу, но так нельзя по правилам библиотеки
    // если же человек - филолог (данная характеристика случаным образом выдается человеку)
    // то он может брать в библиотеке сколько угодно книг
    Diana.take_book(move(b2));
    // Диана все поняла и теперь возвращает первую книгу
    Diana.return_book(b1);
    // тут в библиотеке появляется новый читатель
    Reader Rina = Reader();
    // Рина берет вторую книгу
    Rina.take_book(move(b2));
    // Диана уже собирается взять вторую книгу, но она занята
    Diana.take_book(move(b2));
    // Диана берет третью книгу
    Diana.take_book(move(b3));
    // а потом она одолжила вторую книгу у Рины. Забыв, что это не ее, пытается вернуть в библиотеку
    Diana.return_book(b2);
    // Диана не может вспомнить, какую же книгу она брала, поэтому смотрит в карточку читателя
    Diana.get_last_inf();
    // узнав, что нужно вернуть, она приносит третью книгу
    Diana.return_book(b3);
    // посмотрим, что происходило потом
    Diana.take_book(move(b4));
    Diana.return_book(b4);
    Diana.take_book(move(b5));
    Diana.return_book(b5);
    Diana.take_book(move(b6));
    Diana.return_book(b6);
    Diana.take_book(move(b7));
    Diana.return_book(b7);
    Diana.take_book(move(b8));
    Diana.return_book(b8);
    Diana.take_book(move(b9));
    Diana.return_book(b9);
    // Рина возвращает вторую книгу
    Rina.return_book(b2);
    Diana.take_book(move(b2));
    Diana.return_book(b2);
    Diana.take_book(move(b1));
    Diana.return_book(b1);
    Diana.take_book(move(b7));
    Diana.return_book(b7);
    Diana.take_book(move(b6));
    Diana.return_book(b6);
    Diana.take_book(move(b9));
    Diana.return_book(b9);
    Diana.take_book(move(b2));
    Diana.return_book(b2);
    Diana.take_book(move(b4));
    Diana.return_book(b4);
    Diana.take_book(move(b5));
    Diana.return_book(b5);
    // можно заметить, что на Диану завели новую книгу читателя 
    cout << (last(Diana) + 1) << endl;
    // предположим, что с пятой книгой что-то случилось
    // по правилам библиотеки человек должен купить такую же
    // копируем 5-ую книгу
    Book b10 = Book(b5);
    // как мы видим, Диана спокойно возвращает книгу, сообщения о том, что книга не та, не последовало
    Diana.return_book(b10);
    //проверим, филолог ли Рина
    cout << Rina.isPhilologist() << endl;
    //Рина тоже не филолог, но мы сделаем Татьяну Викторовну, она-то уж точно филолог
    Reader Tatyana = Reader();
    cout << Tatyana.isPhilologist() << endl;
    //теперь удостоверимся, что она может взять несколько книг
    Tatyana.take_book(move(b8));
    Tatyana.take_book(move(b4));
    //как мы видим, никакого сообщения о возврате книги не было выведено
    //посмотрим, являются ли читатели одновременно людьми
    Diana.Live();
    Rina.Laugh();
    Tatyana.Love();
    //в нашей библиотеке появились электронные книги!
    eBook b0 = eBook(1090, "Lomachinsky", "Medical Examiner's Stories", 84);
    Diana.take_book(move(b0));
}
